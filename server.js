const pg = require("pg");
const express = require('express');
const app = express();
const path = require('path');
const connectionString = "pg://postgres:123@localhost:5432/cccccm";
const client = new pg.Client(connectionString);
const bodyParser = require("body-parser");
const random = require('random');
const code_len = 5;

client.connect();
app.listen(8080);
console.log('App started');

let text = "";
let tmp = "";
let string = "";

function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  for (var i = 0; i < code_len; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

console.log(makeid());



app.get('/g/*', async (request, response) => {
  //console.log(req.protocol);
  client.query('SELECT * FROM links WHERE output_links = $1', [request.url], (err, res) => {
    if (err) throw err; 
    string = (res.rows[0].input_links).split(':');
    if (string[0] == 'http' || string[0] == 'https')
      response.redirect(res.rows[0].input_links);
    else response.redirect('http://' + res.rows[0].input_links);

    //response.redirect('https://vk.com');
  });
});


app.get('/', function (request, response) {
  response.sendFile(path.join(__dirname + '/index.html'));
});



app.use(bodyParser.json());
app.post("/save", async (request, response) => {

  console.log(request.body);
  console.log(request.headers['user-agent']);

  if (!request.body) return response.sendStatus(400);
  tmp = `/g/${makeid()}`;

  client.query("insert into links (input_links, output_links, date, user_agent) values" +  
    "($1, $2, $3, $4);", [request.body.url, tmp, (new Date()), request.headers['user-agent']]); 

  text = { 'url': `http://localhost:8080${tmp}`};
  response.send(text);

});









